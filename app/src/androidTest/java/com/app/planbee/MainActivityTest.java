package com.app.planbee;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.app.planbee.ui.user.UserActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by hyochan on 2016. 10. 9..
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class MainActivityTest extends ActivityInstrumentationTestCase2<UserActivity> {
  private static final String TAG = "MainActivityTest";
  private UserActivity mainActivity;

  public MainActivityTest() {
    super(UserActivity.class);
  }

  @Before
  protected void setUp() throws Exception {
    super.setUp();
    injectInstrumentation(InstrumentationRegistry.getInstrumentation());
    mainActivity = getActivity();
  }

  @Test
  public void user_Displayed(){
    // pager가 잘 보이는지 확인
    onView(withId(R.id.pager)).check(ViewAssertions.matches(isDisplayed()));

  }
}

package com.app.planbee.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.planbee.model.Post;
import com.app.planbee.ui.detail.PostDetailFragment;

import java.util.ArrayList;
import java.util.List;

public class DetailViewPagetAdapter extends FragmentStatePagerAdapter {

  private List<Post> posts;

  public DetailViewPagetAdapter(FragmentManager fm) {
    super(fm);
    posts = new ArrayList<>();
  }

  public void setPosts(ArrayList<Post> posts) {
    this.posts = posts;
    notifyDataSetChanged();
  }

  @Override
  public Fragment getItem(int position) {
    return PostDetailFragment.newInstance(posts.get(position));
  }

  @Override
  public int getCount() {
    return posts.size();
  }
}

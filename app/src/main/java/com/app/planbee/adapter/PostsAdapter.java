/**
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2015 Carlos Piñan
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.app.planbee.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.planbee.R;
import com.app.planbee.model.Post;
import com.app.planbee.util.ImageLoader;
import com.app.planbee.util.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsAdapter extends LoadMoreBaseAdapter<Post, PostsAdapter.PostViewHolder> {
  private static final String TAG = "PostsAdapter";
  private PostItemListener mItemListener;

  public PostsAdapter(ArrayList<Post> posts, PostItemListener itemListener) {
    setList(posts);
    mItemListener = itemListener;
  }

  @Override
  public RecyclerView.ViewHolder onCreateDataViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);
    View postView = inflater.inflate(R.layout.row_iamge_item, parent, false);
    return new PostViewHolder(postView, mItemListener);
  }

  @Override
  public void onBindDataViewHolder(RecyclerView.ViewHolder holder, int position) {
    if (getItemViewType(position) == VIEW_TYPE_DATA) {
      ((PostViewHolder) holder).bindPostViewHolder(data.get(position));
    }
  }

  public void replaceData(ArrayList<Post> posts) {
    data.clear();
    setList(posts);
    notifyDataSetChanged();
  }

  private void setList(ArrayList<Post> posts) {
    this.data = posts;
  }

  public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.main_image)
    ImageView imageView;
    @BindView(R.id.title_text)
    TextView titleText;
    @BindView(R.id.image_size_text)
    TextView imageSizeText;
    @BindView(R.id.date_text)
    TextView dateText;
    @BindView(R.id.image_url_text)
    TextView imageUrlText;

    private PostItemListener mItemListener;

    public PostViewHolder(View itemView, PostItemListener listener) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      mItemListener = listener;
      itemView.setOnClickListener(this);
    }

    public void bindPostViewHolder(Post post) {
      if (Util.isValidString(post.getUrl())) {
        ImageLoader.load(imageView, post.getUrl());
        imageSizeText.setText(post.getWidth() + "x" + post.getHeight());
        imageUrlText.setText(post.getUrl());

        imageView.setVisibility(View.VISIBLE);
        imageSizeText.setVisibility(View.VISIBLE);
        imageUrlText.setVisibility(View.VISIBLE);
      } else {
        imageView.setVisibility(View.GONE);
        imageSizeText.setVisibility(View.GONE);
        imageUrlText.setVisibility(View.GONE);
      }

      titleText.setText(post.getTitle());
      dateText.setText(post.getDate());
    }

    @Override
    public void onClick(View v) {
      int position = getAdapterPosition();
      mItemListener.onPostClick(data, position);
    }
  }

  public interface PostItemListener {
    void onPostClick(ArrayList<Post> posts, int clickedPosition);
  }
}

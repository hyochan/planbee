package com.app.planbee.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.planbee.ui.main.MyAlarmFragment;
import com.app.planbee.ui.user.asset.AssetFragment;
import com.app.planbee.ui.user.savemoney.SaveMoneyFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {
  private static final String TAG = "MainPagerAdapter";


  public MainPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    switch (position) {
      case 0:
      default:
        return new MyAlarmFragment();
      case 1:
        return new AssetFragment();
      case 2:
        return new SaveMoneyFragment();
    }
  }

  @Override
  public int getCount() {
    return 3;
  }


  @Override
  public CharSequence getPageTitle(int position) {
    return null;
  }
}

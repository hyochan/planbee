package com.app.planbee.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.app.planbee.ui.user.asset.AssetFragment;
import com.app.planbee.ui.user.savemoney.SaveMoneyFragment;

/**
 * Created by hyochan on 2016. 10. 6..
 */
public class UserMainPagerAdapter extends FragmentStatePagerAdapter {
  private static final String TAG = "UserMainPagerAdapter";

  public UserMainPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    switch (position) {
      case 0:
      default:
        return new SaveMoneyFragment();
      case 1:
        return new AssetFragment();
    }
  }

  @Override
  public int getCount() {
    return 2;
  }
}

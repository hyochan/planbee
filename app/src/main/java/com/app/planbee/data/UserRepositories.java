package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.networking.services.UserServiceAPI;
import com.app.planbee.networking.services.UserServiceAPIImp;

public class UserRepositories {

  private static UserRepository repository = null;

  private UserRepositories() {
  }

  public synchronized static UserRepository getPostManager(@NonNull UserServiceAPI userServiceAPI) {
    if (repository == null) {
      repository = new UserManager(userServiceAPI);
    }
    return repository;
  }

  public synchronized static void changeOffLineRepository(boolean online) {
    ((UserManager) repository).switchAPILayer(online ? new UserServiceAPIImp() : new UserServiceAPIImp());
  }
}


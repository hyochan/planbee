package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.model.Post;

import java.util.ArrayList;

public interface PostRepository {

  interface LoadPostsCallback {
    void onPostsLoaded(ArrayList<Post> posts);
  }

  void getPosts(@NonNull LoadPostsCallback callback, boolean loadMore, boolean forceUpdate);
}

package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.model.User;

public interface UserRepository {

  interface LoadUserCallback {
    void onUserLoaded(User user);
  }

  void getUser(@NonNull LoadUserCallback callback, boolean forceUpdate);
}

package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.model.User;
import com.app.planbee.networking.services.UserServiceAPI;

public class UserManager implements UserRepository {

  private UserServiceAPI mUserServiceAPI;

  public UserManager(@NonNull UserServiceAPI userServiceAPI) {
    mUserServiceAPI = userServiceAPI;
  }

  @Override
  public void getUser(@NonNull LoadUserCallback callback, boolean forceUpdate) {
    mUserServiceAPI.getUser(new UserServiceAPI.ServiceCallback<User>() {
      @Override
      public void onLoaded(User data) {

      }
    });
  }

  public void switchAPILayer(@NonNull UserServiceAPI userServiceAPI) {
    this.mUserServiceAPI = userServiceAPI;
  }
//  @Override
//  public void getUser(@NonNull final LoadPostsCallback callback, boolean loadMore, boolean forceUpdate) {
//    if (forceUpdate) {
////      mPosts.clear();
//    }
//    if (!loadMore && !mPosts.isEmpty()) {
//      callback.onPostsLoaded(mPosts);
//    } else {
//      mUserServiceAPI.getUser(new PostServiceAPI.PostServiceCallback<PostMain>() {
//        @Override
//        public void onLoaded(PostMain posts) {
//          mPosts.addAll(posts.getUser());
//          callback.onPostsLoaded(mPosts);
//        }
//      }, mPosts.size());
//    }
//  }

}

package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.model.Post;
import com.app.planbee.model.PostMain;
import com.app.planbee.networking.services.PostServiceAPI;

import java.util.ArrayList;

public class PostManager implements PostRepository {

  private PostServiceAPI mPostServiceAPI;
  private ArrayList<Post> mPosts = new ArrayList<>();

  public PostManager(@NonNull PostServiceAPI postServiceAPI) {
    mPostServiceAPI = postServiceAPI;
  }

  @Override
  public void getPosts(@NonNull final LoadPostsCallback callback, boolean loadMore, boolean forceUpdate) {
    if (forceUpdate) {
      mPosts.clear();
    }
    if (!loadMore && !mPosts.isEmpty()) {
      callback.onPostsLoaded(mPosts);
    } else {
      mPostServiceAPI.getPosts(new PostServiceAPI.PostServiceCallback<PostMain>() {
        @Override
        public void onLoaded(PostMain posts) {
          mPosts.addAll(posts.getPosts());
          callback.onPostsLoaded(mPosts);
        }
      });
    }
  }
  public void switchAPILayer(@NonNull PostServiceAPI postServiceAPI) {
    this.mPostServiceAPI = postServiceAPI;
  }
}

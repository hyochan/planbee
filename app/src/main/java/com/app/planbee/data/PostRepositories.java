package com.app.planbee.data;

import android.support.annotation.NonNull;

import com.app.planbee.networking.services.PostRealmAPIImp;
import com.app.planbee.networking.services.PostServiceAPI;
import com.app.planbee.networking.services.PostServiceAPIImp;

public class PostRepositories {

  private static PostRepository repository = null;

  private PostRepositories() {
  }

  public synchronized static PostRepository getPostManager(@NonNull PostServiceAPI postServiceAPI) {
    if (repository == null) {
      repository = new PostManager(postServiceAPI);
    }
    return repository;
  }
  public synchronized static void changeOffLineRepository(boolean online) {
    ((PostManager) repository).switchAPILayer(online ? new PostServiceAPIImp() : new PostRealmAPIImp());
  }
}


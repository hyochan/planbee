package com.app.planbee.model;

import com.app.planbee.util.RealmManager;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by hyochan on 2016. 10. 12..
 */
public class PostRealm extends RealmObject {
  private static final String TAG = "PostRealm";

  private String title;

  private String date;

  private String url;
  private int width;
  private int height;

  public PostRealm() {

  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public static List<PostRealm> getPostsInDatabase() {
    return RealmManager.getInstance().getRealmInstance().where(PostRealm.class)
        .findAll();
  }
}

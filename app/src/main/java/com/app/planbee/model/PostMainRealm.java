package com.app.planbee.model;

import com.app.planbee.util.RealmManager;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by hyochan on 2016. 10. 12..
 */
public class PostMainRealm extends RealmObject {
  private static final String TAG = "PostMainRealm";

  private String stat;
  private int page;
  private int totalPage;

  private RealmList<PostRealm> posts = new RealmList<>();

  public PostMainRealm() {

  }

  public String getStat() {
    return stat;
  }

  public void setStat(String stat) {
    this.stat = stat;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public RealmList<PostRealm> getPosts() {
    return posts;
  }

  public void setPosts(RealmList<PostRealm> posts) {
    this.posts = posts;
  }

  public static PostMainRealm getPostMainInDatabase() {
    return RealmManager.getInstance().getRealmInstance().where(PostMainRealm.class)
        .findFirst();
  }
}

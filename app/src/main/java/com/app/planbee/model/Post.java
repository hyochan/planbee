package com.app.planbee.model;

import com.app.planbee.util.RealmManager;
import com.app.planbee.util.Util;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Post {

  private String title;

  @SerializedName("date_taken")
  private String date;

  private String url;
  private int width;
  private int height;

  public Post() {
  }

//  public Post(Parcel in) {
//    readFromParcel(in);
//  }

  public String getTitle() {
    return title;
  }

  public String getDate() {
    return Util.getDateString(date);
  }

  public String getUrl() {
    return url;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public PostRealm saveToRealm() {
    PostRealm postRealm = new PostRealm();
    postRealm.setTitle(title);
    postRealm.setDate(date);
    postRealm.setUrl(url);
    postRealm.setWidth(width);
    postRealm.setHeight(height);
    return postRealm;
  }

  public static void savePostsToRealm(List<Post> posts) {
    List<PostRealm> postRealms = new ArrayList<>();
    for (Post post : posts) {
      postRealms.add(post.saveToRealm());
    }
    RealmManager.getInstance().save(postRealms, PostRealm.class);
  }

  public static List<Post> obtainFromResult(List<PostRealm> postRealms) {
    List<Post> postList = new ArrayList<>();
    for (PostRealm postRealm : postRealms) {
      Post post = new Post();
      post.setTitle(postRealm.getTitle());
      post.setDate(postRealm.getDate());
      post.setUrl(postRealm.getUrl());
      post.setWidth(postRealm.getWidth());
      post.setHeight(postRealm.getHeight());
      postList.add(post);
    }
    return postList;
  }
//  @Override
//  public int describeContents() {
//    return 0;
//  }
//
//  @Override
//  public void writeToParcel(Parcel dest, int flags) {
//    dest.writeString(title);
//    dest.writeString(date);
//    dest.writeString(url);
//    dest.writeInt(width);
//    dest.writeInt(height);
//  }
//
//  private void readFromParcel(Parcel in) {
//    title = in.readString();
//    date = in.readString();
//    url = in.readString();
//    width = in.readInt();
//    height = in.readInt();
//  }
//
//  public static final Creator CREATOR = new Creator() {
//    public Post createFromParcel(Parcel src) {
//      return new Post(src);
//    }
//
//    public Post[] newArray(int size) {
//      return new Post[size];
//    }
//  };
}

package com.app.planbee.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PostMain{

  private String stat;
  private int page;
  @SerializedName("total_page")
  private int totalPage;

  @SerializedName("photos")
  private List<Post> posts = new ArrayList<>();

  public String getStat() {
    return stat;
  }

  public int getPage() {
    return page;
  }

  public int getTotalPage() {
    return totalPage;
  }

  public List<Post> getPosts() {
    return posts;
  }

  public void setStat(String stat) {
    this.stat = stat;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public void setPosts(List<Post> posts) {
    this.posts = posts;
  }

//  public PostMainRealm saveToRealm() {
//    PostMainRealm postMainRealm = new PostMainRealm();
//    postMainRealm.setStat(stat);
//    postMainRealm.setPage(page);
//    postMainRealm.setTotalPage(totalPage);
//    postMainRealm.setPosts(posts);
//    return postMainRealm;
//  }
//
//  public static void savePostsToRealm(PostMain postMain) {
//    PostMainRealm postMainRealm = postMain.saveToRealm();
//    RealmManager.getInstance().save(postMainRealm, PostMainRealm.class);
//  }
//
//  public static PostMain obtainFromResult(PostMainRealm postMainRealm) {
//    PostMain postMain = new PostMain();
//    postMain.setStat(postMainRealm.getStat());
//    postMain.setPage(postMainRealm.getPage());
//    postMain.setTotalPage(postMainRealm.getTotalPage());
//    postMain.setPosts(new RealmList<PostRealm>(postMainRealm.getPosts()));
//    return postMain;
//  }
}

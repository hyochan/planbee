package com.app.planbee;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {

  private static Context mContext;

  @Override
  public void onCreate() {
    super.onCreate();
    mContext = this;
    RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
    Realm.setDefaultConfiguration(realmConfiguration);
  }

  public static Context getContext() {
    return mContext;
  }

}

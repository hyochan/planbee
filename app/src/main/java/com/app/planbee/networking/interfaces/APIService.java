package com.app.planbee.networking.interfaces;

import com.app.planbee.model.PostMain;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by hyochan on 2016. 10. 11..
 */

public interface APIService {
  static final String prefix = "/api/v1";

  @GET("images")
  Call<PostMain> getImagePosts();
}

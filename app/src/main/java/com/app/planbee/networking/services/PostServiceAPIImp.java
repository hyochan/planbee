package com.app.planbee.networking.services;


import com.app.planbee.model.PostMain;
import com.app.planbee.util.API;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostServiceAPIImp implements PostServiceAPI {

  @Override
  public void getPosts(final PostServiceCallback<PostMain> callback) {
    Call<PostMain> callListPosts = API.get().getRetrofitService().getImagePosts();
    callListPosts.enqueue(new Callback<PostMain>() {
      @Override
      public void onResponse(Call<PostMain> call, Response<PostMain> response) {
        if (callback != null) {
          callback.onLoaded(response.body());
        }
      }

      @Override
      public void onFailure(Call<PostMain> call, Throwable t) {
      }
    });
  }
}

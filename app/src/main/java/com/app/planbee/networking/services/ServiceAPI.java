package com.app.planbee.networking.services;


public interface ServiceAPI {

  interface ServiceCallback<T> {
    void onLoaded(T data);
  }
}

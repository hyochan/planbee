package com.app.planbee.networking.services;


import com.app.planbee.model.PostMain;

public interface PostServiceAPI {

  interface PostServiceCallback<T> {
    void onLoaded(T posts);
  }

  void getPosts(final PostServiceCallback<PostMain> callback);
}

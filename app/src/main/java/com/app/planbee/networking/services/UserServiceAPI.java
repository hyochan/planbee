package com.app.planbee.networking.services;


import com.app.planbee.model.User;

public interface UserServiceAPI extends ServiceAPI {

  void getUser(final ServiceCallback<User> callback);
}

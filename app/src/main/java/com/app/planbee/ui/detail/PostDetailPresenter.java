package com.app.planbee.ui.detail;

import android.support.annotation.NonNull;

import com.app.planbee.model.Post;


public class PostDetailPresenter implements PostsDetailContractor.PostsDetailPresenter<PostsDetailContractor.PostDetailView> {

  private PostsDetailContractor.PostDetailView mPostDetailView;
  private Post mPost;

  public PostDetailPresenter(@NonNull Post post) {
    mPost = post;
  }

  @Override
  public void loadPost() {
    mPostDetailView.showPosts(mPost);
  }

  @Override
  public void attachView(PostsDetailContractor.PostDetailView view) {
    this.mPostDetailView = view;
  }

  @Override
  public void detachView() {
    mPostDetailView = null;
  }

}

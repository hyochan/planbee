package com.app.planbee.ui.common;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;

import butterknife.ButterKnife;

public abstract class AbsFragment<T extends MvpPresenter> extends Fragment implements MvpView {
  private static final String TAG = "AbsFragment";

  protected T mPresenter;
  protected ProgressDialog mProgressDialog;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = getContentView(inflater, container, savedInstanceState);
    ButterKnife.bind(this, view);
    return view;
  }

  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    initView(savedInstanceState);
    if (mPresenter != null) {
      mPresenter.attachView(this);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    setViewData();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    if (mPresenter != null) {
      mPresenter.detachView();
    }
  }

  @Override
  public void setProgressIndicator(boolean active) {
    if (active) {
      if (mProgressDialog == null) {
        mProgressDialog = new ProgressDialog(getActivity().getApplicationContext());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(null);
        mProgressDialog.setMessage(getString(R.string.loading));
      } else {
        mProgressDialog.show();
      }
    } else {
      mProgressDialog.hide();
    }
  }

  protected abstract View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

  protected abstract void initView(Bundle savedInstanceState);

  protected abstract void setViewData();
}

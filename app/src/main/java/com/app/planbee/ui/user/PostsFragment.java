package com.app.planbee.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;
import com.app.planbee.adapter.PostsAdapter;
import com.app.planbee.data.PostRepositories;
import com.app.planbee.data.PostRepository;
import com.app.planbee.model.Post;
import com.app.planbee.networking.services.PostRealmAPIImp;
import com.app.planbee.networking.services.PostServiceAPIImp;
import com.app.planbee.ui.common.AbsFragment;
import com.app.planbee.ui.custom.EndlessRecyclerOnScrollListener;
import com.app.planbee.util.API;

import java.util.ArrayList;

import butterknife.BindView;

public class PostsFragment extends AbsFragment<PostsContractor.PostsPresenter> implements PostsContractor.PostsView, PostsAdapter.PostItemListener {

  @BindView(R.id.refresh_layout)
  SwipeRefreshLayout refreshLayout;

  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;

  private PostsAdapter mPostAdapter;
  private EndlessRecyclerOnScrollListener mEndlessRecyclerOnScrollListener;

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_recycler, container, false);
  }

  private PostRepository provideNotesRepository() {
    return PostRepositories.getPostManager(API.isConnected() ? new PostServiceAPIImp() : new PostRealmAPIImp());
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    mPresenter = new PostsPresenter(provideNotesRepository());

    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(linearLayoutManager);
    mEndlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
      @Override
      public void onLoadMore(int current_page) {
        if (!mPostAdapter.isLoading()) {
          mPresenter.loadPosts(true, false);
        }
      }
    };

    recyclerView.addOnScrollListener(mEndlessRecyclerOnScrollListener);
    refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        mEndlessRecyclerOnScrollListener.reset();
        mPresenter.loadPosts(false, true);
      }
    });
  }

  @Override
  protected void setViewData() {
    mPresenter.loadPosts(false, false);
  }

  @Override
  public void showPostsLoading(boolean loading) {
    if (mPostAdapter != null) {
      mPostAdapter.setLoading(loading);
    }
  }

  @Override
  public void onPostClick(ArrayList<Post> posts, int clickedPosition) {
    mPresenter.openPostDetails(posts, clickedPosition);
  }

  @Override
  public void showPostDetailUi(ArrayList<Post> posts, int clickPostion) {
//    Intent intent = new Intent(getActivity(), POac.class);
//    intent.putParcelableArrayListExtra(PostDetailActivity.DATA, posts);
//    intent.putExtra(PostDetailActivity.POSITION, clickPostion);
//    startActivity(intent);
  }

  @Override
  public void showPosts(ArrayList<Post> posts) {
    refreshLayout.setRefreshing(false);
    if (mPostAdapter == null) {
      mPostAdapter = new PostsAdapter(new ArrayList<>(posts), this);
      recyclerView.setAdapter(mPostAdapter);
    }
    mPostAdapter.setLoading(false);
    mPostAdapter.replaceData(new ArrayList<>(posts));
  }

}

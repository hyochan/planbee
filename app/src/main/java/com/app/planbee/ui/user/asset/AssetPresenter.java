package com.app.planbee.ui.user.asset;

import android.support.annotation.NonNull;

import com.app.planbee.model.Asset;

public class AssetPresenter implements AssetContractor.AssetPresenter<AssetContractor.AssetView> {

  private AssetContractor.AssetView mAssetView;
  private Asset mAsset;


  public AssetPresenter(@NonNull Asset asset) {
    mAsset = asset;
  }

  @Override
  public void loadAsset() {
    mAssetView.showAsset(mAsset);
  }

  @Override
  public void attachView(AssetContractor.AssetView view) {
    this.mAssetView = view;
  }

  @Override
  public void detachView() {
    mAssetView = null;
  }

}

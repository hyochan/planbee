package com.app.planbee.ui.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;
import com.app.planbee.data.PostRepositories;
import com.app.planbee.data.PostRepository;
import com.app.planbee.model.Post;
import com.app.planbee.networking.services.PostRealmAPIImp;
import com.app.planbee.networking.services.PostServiceAPIImp;
import com.app.planbee.ui.common.AbsFragment;
import com.app.planbee.util.API;

import java.util.ArrayList;

import butterknife.BindView;

public class MyAlarmFragment extends AbsFragment<MyAlarmContractor.MyAlarmPresenter> implements MyAlarmContractor.MyAlarmView {

  @BindView(R.id.refresh_layout)
  SwipeRefreshLayout refreshLayout;

  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_recycler, container, false);
  }

  private PostRepository provideNotesRepository() {
    return PostRepositories.getPostManager(API.isConnected() ? new PostServiceAPIImp() : new PostRealmAPIImp());
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    mPresenter = new MyAlarmPresenter(provideNotesRepository());

    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(linearLayoutManager);
    recyclerView.setBackgroundColor(Color.RED);
    refreshLayout.setEnabled(false);
  }

  @Override
  protected void setViewData() {
    mPresenter.loadPosts(false, false);
  }

  @Override
  public void showPostsLoading(boolean loading) {
  }

  public void onPostClick(ArrayList<Post> posts, int clickedPosition) {
    mPresenter.openPostDetails(posts, clickedPosition);
  }

  @Override
  public void showPostDetailUi(ArrayList<Post> posts, int clickPostion) {
//    Intent intent = new Intent(getActivity(), POac.class);
//    intent.putParcelableArrayListExtra(PostDetailActivity.DATA, posts);
//    intent.putExtra(PostDetailActivity.POSITION, clickPostion);
//    startActivity(intent);
  }

  @Override
  public void showPosts(ArrayList<Post> posts) {
  }

}

package com.app.planbee.ui.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class AbsActivity extends AppCompatActivity {
  private static final String TAG = "AbsActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getContentView());
    ButterKnife.bind(this);

  }

  protected abstract int getContentView();
}

package com.app.planbee.ui.user.savemoney;

import com.app.planbee.model.SaveMoney;
import com.app.planbee.ui.common.MvpPresenter;
import com.app.planbee.ui.common.MvpView;

public interface SaveMoneyContractor {

  interface SaveMoneyPresenter<View> extends MvpPresenter<View> {
    void loadSaveMoney();
  }

  interface SaveMoneyView extends MvpView {
    void showSaveMoney(SaveMoney saveMoney);
  }
}


package com.app.planbee.ui.custom;

import android.content.Context;
import android.widget.TextView;

import com.app.planbee.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaveMoneyMarkerView extends MarkerView {

  @BindView(R.id.tvContent)
  TextView tvContent;

  public SaveMoneyMarkerView(Context context, int layoutResource) {
    super(context, layoutResource);
    ButterKnife.bind(this);
  }

  @Override
  public void refreshContent(Entry e, Highlight highlight) {
    if (e instanceof CandleEntry) {
      CandleEntry ce = (CandleEntry) e;
      tvContent.setText(String.valueOf(Utils.formatNumber(ce.getHigh(), 0, true)));
    } else {
      tvContent.setText(String.valueOf(Utils.formatNumber(e.getY(), 0, true)));
    }
  }

  @Override
  public int getXOffset(float xpos) {
    // this will center the marker-view horizontally
    return -(getWidth() / 2);
  }

  @Override
  public int getYOffset(float ypos) {
    // this will cause the marker-view to be above the selected value
    return -getHeight();
  }
}

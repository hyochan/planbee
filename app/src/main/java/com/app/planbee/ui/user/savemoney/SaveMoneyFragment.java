package com.app.planbee.ui.user.savemoney;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;
import com.app.planbee.model.SaveMoney;
import com.app.planbee.ui.common.AbsFragment;
import com.app.planbee.ui.custom.SaveMoneyMarkerView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by hyochan on 2016. 10. 3..
 */
public class SaveMoneyFragment extends AbsFragment<SaveMoneyContractor.SaveMoneyPresenter> implements SaveMoneyContractor.SaveMoneyView {
  private static final String TAG = "SaveMoneyFragment";
  protected String[] mParties = new String[]{
      "적금", "예금", "보험", "펀드", "주식", "현금", "용돈"
  };

  @BindView(R.id.radar_chart)
  RadarChart mRadarChart;

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_save_money, container, false);
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    initChart();
  }

  private void initChart() {
    mRadarChart.setDescription("dsdsd");
    mRadarChart.setDescriptionPosition(0.5f, 0f);

    mRadarChart.setWebLineWidth(1f);
    mRadarChart.setWebColor(Color.LTGRAY);
    mRadarChart.setWebLineWidthInner(1f);
    mRadarChart.setWebColorInner(Color.LTGRAY);
    mRadarChart.setWebAlpha(100);
    mRadarChart.setRotationEnabled(false);

    SaveMoneyMarkerView markerView = new SaveMoneyMarkerView(getContext(), R.layout.custom_marker_view);
    mRadarChart.setMarkerView(markerView);

    mRadarChart.animateXY(
        1400, 1400,
        Easing.EasingOption.EaseInOutQuad,
        Easing.EasingOption.EaseInOutQuad);

    XAxis xAxis = mRadarChart.getXAxis();
    xAxis.setTextSize(9f);
    xAxis.setYOffset(0f);
    xAxis.setXOffset(0f);
    xAxis.setValueFormatter(new AxisValueFormatter() {
      @Override
      public String getFormattedValue(float value, AxisBase axis) {
        return mParties[(int) value % mParties.length];
      }

      @Override
      public int getDecimalDigits() {
        return 0;
      }
    });
    xAxis.setTextColor(Color.WHITE);

    YAxis yAxis = mRadarChart.getYAxis();
    yAxis.setLabelCount(5, false);
    yAxis.setTextSize(9f);
    yAxis.setDrawLabels(false);

    mRadarChart.getLegend().setEnabled(false);
  }

  @Override
  protected void setViewData() {
    setData();
    mRadarChart.invalidate();
  }

  @Override
  public void showSaveMoney(SaveMoney saveMoney) {

  }

  public void setData() {

    float min = 10;
    float mult = 150;
    int cnt = mParties.length;

    ArrayList<RadarEntry> entries1 = new ArrayList<RadarEntry>();

    // NOTE: The order of the entries when being added to the entries array determines their position around the center of
    // the chart.
    for (int i = 0; i < cnt; i++) {
      float val1 = (float) (Math.random() * mult) + min;
      entries1.add(new RadarEntry(val1));

    }

    RadarDataSet set1 = new RadarDataSet(entries1, "Me");
    set1.setColor(ColorTemplate.VORDIPLOM_COLORS[1]);
    set1.setFillColor(ColorTemplate.VORDIPLOM_COLORS[1]);
    set1.setDrawFilled(true);
    set1.setFillAlpha(180);
    set1.setLineWidth(2f);
    set1.setDrawHighlightCircleEnabled(true);
    set1.setDrawHighlightIndicators(false);

    ArrayList<IRadarDataSet> sets = new ArrayList<IRadarDataSet>();
    sets.add(set1);

    RadarData data = new RadarData(sets);
    data.setValueTextSize(8f);
    data.setDrawValues(false);
    data.setValueTextColor(ColorTemplate.MATERIAL_COLORS[0]);

    mRadarChart.setData(data);

    mRadarChart.invalidate();
  }
}

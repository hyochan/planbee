package com.app.planbee.ui.user.savemoney;

import android.support.annotation.NonNull;

import com.app.planbee.model.SaveMoney;

public class SaveMoneyPresenter implements SaveMoneyContractor.SaveMoneyPresenter<SaveMoneyContractor.SaveMoneyView> {

  private SaveMoneyContractor.SaveMoneyView saveMoneyView;
  private SaveMoney mSaveMoney;

  public SaveMoneyPresenter(@NonNull SaveMoney saveMoney) {
    mSaveMoney = saveMoney;
  }

  @Override
  public void loadSaveMoney() {
    saveMoneyView.showSaveMoney(mSaveMoney);
  }

  @Override
  public void attachView(SaveMoneyContractor.SaveMoneyView view) {
    this.saveMoneyView = view;
  }

  @Override
  public void detachView() {
    saveMoneyView = null;
  }

}

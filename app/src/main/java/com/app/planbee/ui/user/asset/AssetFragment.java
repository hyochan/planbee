package com.app.planbee.ui.user.asset;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;
import com.app.planbee.model.Asset;
import com.app.planbee.ui.common.AbsFragment;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by hyochan on 2016. 10. 3..
 */
public class AssetFragment extends AbsFragment<AssetContractor.AssetPresenter> implements AssetContractor.AssetView, OnChartValueSelectedListener {
  private static final String TAG = "AssetFragment";

  protected String[] mParties = new String[] {
      "부동산", "예금", "현물", "현금", "부채", "기타"
  };

  @BindView(R.id.pie_chart)
  PieChart mPieChart;

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_asset, container, false);
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    initChart();
  }

  private void initChart() {
    mPieChart.setUsePercentValues(true);
    mPieChart.setDescription("");
    mPieChart.setDragDecelerationFrictionCoef(1f);

    mPieChart.setCenterText(generateCenterSpannableText());

    mPieChart.setDrawHoleEnabled(true);
    mPieChart.setHoleColor(Color.TRANSPARENT);

    mPieChart.setTransparentCircleColor(Color.WHITE);
    mPieChart.setTransparentCircleAlpha(110);

    mPieChart.setHoleRadius(58f);
    mPieChart.setTransparentCircleRadius(61f);

    mPieChart.setDrawCenterText(true);

    mPieChart.setRotationAngle(0);
    mPieChart.setRotationEnabled(false);
    mPieChart.setHighlightPerTapEnabled(true);

    mPieChart.setOnChartValueSelectedListener(this);

    setData(mParties.length, 100);

    mPieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

    mPieChart.getLegend().setEnabled(false);

    // entry label styling
    mPieChart.setEntryLabelColor(Color.BLACK);
    mPieChart.setEntryLabelTextSize(12f);
  }

  @Override
  public void showAsset(Asset asset) {

  }

  @Override
  protected void setViewData() {
    Log.w(TAG, "resume");
    setData(4, 100);
    mPieChart.invalidate();
  }

  private void setData(int count, float range) {

    float mult = range;

    ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

    // NOTE: The order of the entries when being added to the entries array determines their position around the center of
    // the chart.
    for (int i = 0; i < count ; i++) {
      entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5), mParties[i % mParties.length]));
    }

    PieDataSet dataSet = new PieDataSet(entries, "Election Results");
    dataSet.setSliceSpace(3f);
    dataSet.setSelectionShift(5f);

    // add a lot of colors

    ArrayList<Integer> colors = new ArrayList<Integer>();

    for (int c : ColorTemplate.VORDIPLOM_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.JOYFUL_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.COLORFUL_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.LIBERTY_COLORS)
      colors.add(c);

    for (int c : ColorTemplate.PASTEL_COLORS)
      colors.add(c);

    colors.add(ColorTemplate.getHoloBlue());

    dataSet.setColors(colors);
    //dataSet.setSelectionShift(0f);

    PieData data = new PieData(dataSet);
    data.setValueFormatter(new PercentFormatter());
    data.setValueTextSize(11f);
    data.setValueTextColor(Color.BLACK);
    mPieChart.setData(data);

    // undo all highlights
    mPieChart.highlightValues(null);

    mPieChart.invalidate();
  }

  private SpannableString generateCenterSpannableText() {

    SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
    s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
    s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
    s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
    s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
    s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
    s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
    return s;
  }

  @Override
  public void onValueSelected(Entry e, Highlight h) {

  }

  @Override
  public void onNothingSelected() {

  }

}

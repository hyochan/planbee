package com.app.planbee.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.planbee.R;
import com.app.planbee.adapter.UserMainPagerAdapter;
import com.app.planbee.ui.common.AbsFragment;

import butterknife.BindView;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by hyochan on 2016. 10. 3..
 */
public class UserMainFragment extends AbsFragment {
  private static final String TAG = "UserMainFragment";

  @BindView(R.id.pager)
  ViewPager pager;

  @BindView(R.id.indicator)
  CircleIndicator indicator;

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_user_main, container, false);
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    UserMainPagerAdapter adapter = new UserMainPagerAdapter(getChildFragmentManager());
    pager.setAdapter(adapter);
    indicator.setViewPager(pager);
  }

  @Override
  protected void setViewData() {

  }
}

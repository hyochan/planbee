package com.app.planbee.ui.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.planbee.R;
import com.app.planbee.model.Post;
import com.app.planbee.ui.common.AbsFragment;
import com.app.planbee.util.ImageLoader;
import com.app.planbee.util.Util;

import butterknife.BindView;

public class PostDetailFragment extends AbsFragment<PostsDetailContractor.PostsDetailPresenter> implements PostsDetailContractor.PostDetailView {
  private static final String DATA = "data";

  @BindView(R.id.detail_image)
  ImageView imageView;
  @BindView(R.id.detail_title_text)
  TextView titleText;
  @BindView(R.id.detail_image_size_text)
  TextView imageSizeText;
  @BindView(R.id.detail_date_text)
  TextView dateText;
  @BindView(R.id.detail_image_url_text)
  TextView imageUrlText;

  public static PostDetailFragment newInstance(Post post) {
    PostDetailFragment fragment = new PostDetailFragment();
    Bundle args = new Bundle();
//    args.putParcelable(DATA, post);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_detail, container, false);
  }

  @Override
  protected void initView(Bundle savedInstanceState) {
    Bundle bundle = this.getArguments();
    if (bundle != null) {
      if (bundle.getParcelable(DATA) instanceof Post) {
        mPresenter = new PostDetailPresenter((Post) bundle.getParcelable(DATA));
      }
    }

    if (savedInstanceState != null) {
      if (savedInstanceState.getParcelable(DATA) instanceof Post) {
        mPresenter = new PostDetailPresenter((Post) savedInstanceState.getParcelable(DATA));
      }
    }
  }

  @Override
  protected void setViewData() {
    mPresenter.loadPost();
  }

  @Override
  public void showPosts(Post post) {
    if (Util.isValidString(post.getUrl())) {
      ImageLoader.load(imageView, post.getUrl());
      imageSizeText.setText(post.getWidth() + "x" + post.getHeight());
      imageUrlText.setText(post.getUrl());

      imageView.setVisibility(View.VISIBLE);
      imageSizeText.setVisibility(View.VISIBLE);
      imageUrlText.setVisibility(View.VISIBLE);
    } else {
      imageView.setVisibility(View.GONE);
      imageSizeText.setVisibility(View.GONE);
      imageUrlText.setVisibility(View.GONE);
    }

    titleText.setText(post.getTitle());
    dateText.setText(post.getDate());
  }
}

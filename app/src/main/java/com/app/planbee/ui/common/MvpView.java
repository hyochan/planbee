package com.app.planbee.ui.common;

public interface MvpView {
  void setProgressIndicator(boolean active);
}

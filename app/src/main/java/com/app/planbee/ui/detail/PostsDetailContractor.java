package com.app.planbee.ui.detail;


import com.app.planbee.model.Post;
import com.app.planbee.ui.common.MvpPresenter;
import com.app.planbee.ui.common.MvpView;

public interface PostsDetailContractor {

  interface PostsDetailPresenter<View> extends MvpPresenter<View> {
    void loadPost();
  }

  interface PostDetailView extends MvpView {
    void showPosts(Post post);
  }
}

package com.app.planbee.ui.main;

import android.support.annotation.NonNull;

import com.app.planbee.data.PostRepository;
import com.app.planbee.model.Post;

import java.util.ArrayList;


public class MyAlarmPresenter implements MyAlarmContractor.MyAlarmPresenter<MyAlarmContractor.MyAlarmView> {

  private MyAlarmContractor.MyAlarmView myAlarmView;
  private PostRepository mPostsRepository;

  public MyAlarmPresenter(@NonNull PostRepository postRepository) {
    mPostsRepository = postRepository;
  }

  @Override
  public void loadPosts(boolean loadMore, boolean forceUpdate) {
    myAlarmView.showPostsLoading(true);
    mPostsRepository.getPosts(new PostRepository.LoadPostsCallback() {
      @Override
      public void onPostsLoaded(ArrayList<Post> posts) {
        myAlarmView.showPostsLoading(false);
        myAlarmView.showPosts(posts);
      }
    }, loadMore, forceUpdate);
  }

  @Override
  public void openPostDetails(@NonNull ArrayList<Post> posts, int clickPostion) {
    myAlarmView.showPostDetailUi(posts, clickPostion);
  }

  @Override
  public void attachView(MyAlarmContractor.MyAlarmView myAlarmView) {
    this.myAlarmView = myAlarmView;
  }

  @Override
  public void detachView() {
    myAlarmView = null;
  }

}

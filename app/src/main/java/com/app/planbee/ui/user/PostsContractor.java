package com.app.planbee.ui.user;
import android.support.annotation.NonNull;

import com.app.planbee.model.Post;
import com.app.planbee.ui.common.MvpPresenter;
import com.app.planbee.ui.common.MvpView;

import java.util.ArrayList;


public interface PostsContractor {

  interface PostsPresenter<View> extends MvpPresenter<View> {
    void loadPosts(boolean loadMore, boolean forceUpdate);
    void openPostDetails(@NonNull ArrayList<Post> posts, int clickPostion);
  }

  interface PostsView extends MvpView {
    void showPosts(ArrayList<Post> posts);
    void showPostsLoading(boolean loading);
    void showPostDetailUi(ArrayList<Post> posts, int clickPostion);
  }
}

package com.app.planbee.ui.detail;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.app.planbee.R;
import com.app.planbee.adapter.DetailViewPagetAdapter;
import com.app.planbee.ui.common.AbsActivity;

import butterknife.BindView;

public class PostDetailActivity extends AbsActivity {
  public static final String DATA = "data";
  public static final String POSITION = "position";

  @BindView(R.id.toolbar)
  Toolbar toolbar;

  @BindView(R.id.pager)
  ViewPager pager;

  @BindView(R.id.ic_arrow_left)
  ImageView icLeft;

  @BindView(R.id.ic_arrow_right)
  ImageView icRight;

  private DetailViewPagetAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    adapter = new DetailViewPagetAdapter(getSupportFragmentManager());
    pager.setAdapter(adapter);

    if (getIntent() != null) {
//      ArrayList<Post> posts = getIntent().getParcelableArrayListExtra(DATA);
//      int postion = getIntent().getIntExtra(POSITION, 0);
//      adapter.setPosts(posts);
//      pager.setCurrentItem(postion);
//      setArrowIcon(postion);
    }

    pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        setArrowIcon(position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

  private void setArrowIcon(int position) {
    if (position == 0) {
      icLeft.setVisibility(View.INVISIBLE);
      icRight.setVisibility(View.VISIBLE);
    } else if (position == adapter.getCount() - 1) {
      icLeft.setVisibility(View.VISIBLE);
      icRight.setVisibility(View.INVISIBLE);
    } else {
      icLeft.setVisibility(View.VISIBLE);
      icRight.setVisibility(View.VISIBLE);
    }
  }
  @Override
  protected int getContentView() {
    return R.layout.activity_detail;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        supportFinishAfterTransition();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}

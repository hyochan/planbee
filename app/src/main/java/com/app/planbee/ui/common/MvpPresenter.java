package com.app.planbee.ui.common;

public interface MvpPresenter<V> {
  void attachView(V view);

  void detachView();
}
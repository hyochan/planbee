package com.app.planbee.ui.info;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.app.planbee.R;
import com.app.planbee.adapter.MainPagerAdapter;
import com.app.planbee.ui.common.AbsActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class InputInfoActivity extends AbsActivity implements NavigationView.OnNavigationItemSelectedListener {
  private static final String TAG = "InputInfoActivity";

  @BindView(R.id.toolbar)
  Toolbar toolbar;

  @BindView(R.id.collapsing_toolbar)
  CollapsingToolbarLayout collapsingToolbar;

  @BindView(R.id.tab)
  TabLayout tab;

  @BindView(R.id.pager)
  ViewPager pager;

  @BindView(R.id.fab)
  FloatingActionButton fab;

  @OnClick(R.id.fab)
  void submit() {
    startActivity(new Intent(getApplicationContext(), InputInfoActivity.class));
  }

  @Override
  protected int getContentView() {
    return R.layout.app_main;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setSupportActionBar(toolbar);
    collapsingToolbar.setTitle("");

    fab.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);

    initView();
  }

  private void initView() {
    pager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
    tab.setupWithViewPager(pager);
    setupTabIcons();
  }

  private void setupTabIcons() {
    try {
      tab.getTabAt(0).setIcon(R.drawable.ic_menu_camera);
      tab.getTabAt(1).setIcon(R.drawable.ic_menu_gallery);
      tab.getTabAt(2).setIcon(R.drawable.ic_menu_manage);
    } catch (NullPointerException e) {

    }
  }

  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_camera) {
      // Handle the camera action
    } else if (id == R.id.nav_gallery) {

    } else if (id == R.id.nav_slideshow) {

    } else if (id == R.id.nav_manage) {

    } else if (id == R.id.nav_share) {

    } else if (id == R.id.nav_send) {

    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }
}
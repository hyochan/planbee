package com.app.planbee.ui.user;

import android.support.annotation.NonNull;

import com.app.planbee.data.PostRepository;
import com.app.planbee.model.Post;

import java.util.ArrayList;


public class PostsPresenter implements PostsContractor.PostsPresenter<PostsContractor.PostsView> {

  private PostsContractor.PostsView mPostsView;
  private PostRepository mPostsRepository;

  public PostsPresenter(@NonNull PostRepository postRepository) {
    mPostsRepository = postRepository;
  }

  @Override
  public void loadPosts(boolean loadMore, boolean forceUpdate) {
    mPostsView.showPostsLoading(true);
    mPostsRepository.getPosts(new PostRepository.LoadPostsCallback() {
      @Override
      public void onPostsLoaded(ArrayList<Post> posts) {
        mPostsView.showPostsLoading(false);
        mPostsView.showPosts(posts);
      }
    }, loadMore, forceUpdate);
  }

  @Override
  public void openPostDetails(@NonNull ArrayList<Post> posts, int clickPostion) {
    mPostsView.showPostDetailUi(posts, clickPostion);
  }

  @Override
  public void attachView(PostsContractor.PostsView mPostsView) {
    this.mPostsView = mPostsView;
  }

  @Override
  public void detachView() {
    mPostsView = null;
  }

}

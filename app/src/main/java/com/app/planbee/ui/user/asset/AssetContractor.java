package com.app.planbee.ui.user.asset;

import com.app.planbee.model.Asset;
import com.app.planbee.ui.common.MvpPresenter;
import com.app.planbee.ui.common.MvpView;

public interface AssetContractor {

  interface AssetPresenter<View> extends MvpPresenter<View> {
    void loadAsset();
  }

  interface AssetView extends MvpView {
    void showAsset(Asset asset);
  }
}

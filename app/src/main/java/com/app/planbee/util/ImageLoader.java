package com.app.planbee.util;

import android.graphics.Color;
import android.widget.ImageView;

import com.app.planbee.BaseApplication;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ImageLoader {

  public static void load(ImageView view, String imageUrl) {
    Glide.with(BaseApplication.getContext()).load(imageUrl)
        .error(Color.DKGRAY)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .thumbnail(0.2f)
        .crossFade()
        .into(view);
  }

}

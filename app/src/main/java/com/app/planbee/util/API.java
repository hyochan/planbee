package com.app.planbee.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.app.planbee.BaseApplication;
import com.app.planbee.networking.interfaces.APIService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
  private static final String SERVER_DOMAIN = "http://demo2587971.mockable.io/";

  private static API instance;

  private APIService service;

  private API() {
  }

  public static API get() {
    if (instance == null) {
      instance = new API();

    }
    return instance;
  }

  private Retrofit getRetrofit() {
    OkHttpClient client = new OkHttpClient();
//    client.networkInterceptors().add(LoggingInterceptor);
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(SERVER_DOMAIN)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build();
    return retrofit;
  }

  public APIService getRetrofitService() {
    if (service == null) {
      service = getRetrofit().create(APIService.class);
    }
    return service;
  }

  public static boolean isConnected() {
    ConnectivityManager conMgr = (ConnectivityManager) BaseApplication.getContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo i = conMgr.getActiveNetworkInfo();
    if (i == null) {
      return false;
    } else if (!i.isConnected()) {
      return false;
    } else if (!i.isAvailable()) {
      return false;
    } else {
      return true;
    }
  }

}

package com.app.planbee.util;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {
  private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  private static final String PRINT_TIME_FORMAT = "yyyy/MM/dd HH:mm";

  public static String getDateString(String utcDateStr) {
    Date date;
    try {
      date = createDateFormatter(TIME_FORMAT).parse(utcDateStr);
    } catch (Exception e) {
      date =  new Date();
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat(PRINT_TIME_FORMAT, Locale.getDefault());
    return dateFormat.format(date);


  }

  private static SimpleDateFormat createDateFormatter(String pattern) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());

    return dateFormat;
  }

  public static boolean isValidString(String text) {
    return text != null && text.length() > 0 && !text.equals("null") && !(text.replace(" ", "").equals(""));
  }

  public static String getNotNullString(String text) {
    return text != null ? text : "";
  }
}


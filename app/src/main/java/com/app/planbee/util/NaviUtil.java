package com.app.planbee.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.app.planbee.R;
import com.app.planbee.ui.common.AbsActivity;

public class NaviUtil {
  private static final String TAG = NaviUtil.class.getSimpleName();

  public static void initToolbarFragment(AbsActivity activity, Fragment fragment) {
    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.replace(R.id.toolbar_layout, fragment);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public static void initFragment(AbsActivity activity, Fragment fragment) {
    initFragment(activity, fragment, R.id.content_main);
  }

  public static void initFragment(AppCompatActivity activity, Fragment fragment, int id) {
    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.add(id, fragment);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public static void addFragment(Context context, Fragment fragment) {
    addFragment((AppCompatActivity) context, fragment);
  }

  public static void addFragment(AbsActivity activity, Fragment fragment) {
    addFragment(activity, fragment, R.id.content_main);
  }

  public static void addFragment(AppCompatActivity activity, Fragment fragment, int id) {
    if (fragment == null) return;

    String tag = fragment.getClass().getSimpleName();

    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.add(id, fragment, tag);
    fragmentTransaction.setBreadCrumbShortTitle(tag);
    fragmentTransaction.addToBackStack(TAG);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public static void replaceFragment(Context context, Fragment fragment) {
    replaceFragment((AppCompatActivity) context, fragment);
  }

  public static void replaceFragment(AppCompatActivity activity, Fragment fragment) {
    String tag = fragment.getClass().getSimpleName();

    try {
      activity.getSupportFragmentManager().popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    } catch (Exception e) {
      e.fillInStackTrace();
    }
    FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
    fragmentTransaction.add(R.id.content_main, fragment, tag);
    fragmentTransaction.setBreadCrumbShortTitle(tag);
    fragmentTransaction.addToBackStack(TAG);
    fragmentTransaction.commitAllowingStateLoss();
  }

  public static void removeAllFragment(FragmentActivity activity) {
    try {
      FragmentManager fragmentManager = activity.getSupportFragmentManager();
      fragmentManager.popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
      fragmentManager.beginTransaction().commitAllowingStateLoss();
    } catch (Exception e) {
      e.fillInStackTrace();
    }
  }

  public static void removeFragment(AppCompatActivity activity, String lastFragmentTAG) {
    if (activity == null) {
      return;
    }

    final FragmentManager fragmentManager = activity.getSupportFragmentManager();
    for (int i = fragmentManager.getBackStackEntryCount() - 1; i > 0; i--) {
      if (fragmentManager.getBackStackEntryAt(i).getBreadCrumbShortTitle().equals(lastFragmentTAG)) {
        break;
      }
      fragmentManager.popBackStack();
    }
  }




}





